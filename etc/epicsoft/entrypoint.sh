#!/bin/sh

# waiting for Elasticsearch
while ! curl -s http://$BLOONIX_ES_HOST:$BLOONIX_ES_PORT/
do
  echo "$(date) - waiting for Elasticsearch container"
  sleep 1
done
echo "$(date) - Elasticsearch connected successfully"

# waiting for PostgreSQL
while ! curl http://$BLOONIX_DB_HOST:$BLOONIX_DB_PORT/ 2>&1 | grep -q '52'
do
  echo "$(date) - waiting for PostgreSQL container"
  sleep 1
done
echo "$(date) - PostgreSQL connected successfully"

# replace environments
TEMPLATES="/etc/bloonix/database/main.conf.template /etc/bloonix/agent/main.conf.template /etc/bloonix/webgui/main.conf.template /etc/bloonix/server/main.conf.template /etc/bloonix/srvchk/main.conf.template"
for TPL in $TEMPLATES
do
  CONF=${TPL/.template/}
  echo "prepare configuration $CONF"
  go-replace --mode=template $TPL:$CONF
done

# first run of container 
if [ ! -f /etc/bloonix/initialized ]; then
  echo "First start of bloonix, container is now initialized ..."
  /srv/bloonix/webgui/schema/init-elasticsearch $BLOONIX_ES_HOST:$BLOONIX_ES_PORT

  yum -y update && yum -y install bloonix-plugin-config && yum -y clean all
  
  touch /var/log/bloonix/bloonix-webgui.log
  chown -R bloonix:bloonix /var/log/bloonix/
  chown -R bloonix:bloonix /etc/bloonix/
  chown -R bloonix:bloonix /var/lib/bloonix/
  
  echo "$(date)" > /etc/bloonix/initialized
  echo "First start of bloonix completed"
fi

# Bloonix pre-start 
echo "starting Bloonix pre-start"
/usr/lib/bloonix/bin/bloonix-pre-start /var/lib/bloonix /var/lib/bloonix/ipc /var/lib/bloonix/webgui /var/log/bloonix /var/run/bloonix

# Bloonix Web-GUI
echo "starting Bloonix Web-GUI"
/srv/bloonix/webgui/scripts/bloonix-webgui --pid-file /var/run/bloonix/bloonix-webgui.pid --config-file /etc/bloonix/webgui/main.conf

# Bloonix server 
echo "starting Bloonix server"
/usr/bin/bloonix-server --pid-file /var/run/bloonix/bloonix-server.pid --config-file /etc/bloonix/server/main.conf

# Bloonix server-check
echo "starting Bloonix server-check"
/usr/bin/bloonix-srvchk --pid-file /var/run/bloonix/bloonix-srvchk.pid --config-file /etc/bloonix/srvchk/main.conf

# Bloonix Agent 
echo "starting Bloonix server-agent"
/usr/bin/bloonix-agent --pid-file /var/run/bloonix/bloonix-agent.pid --config-file /etc/bloonix/agent/main.conf --sock-file /var/run/bloonix/bloonix-agent.sock

# Nginx Server
echo "starting nginx"
nginx -g 'daemon off;'
