FROM centos:7
LABEL image.name="bloonix_centos" \
      image.description="Docker container for the opensource monitoring software Bloonix, based on CentOS image." \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2017 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV BLOONIX_DB_HOST="postgres" \
    BLOONIX_DB_PORT="5432" \
    BLOONIX_DB_SCHEMA="bloonix" \
    BLOONIX_DB_USER="bloonix" \
    BLOONIX_DB_PASSWORD="bloonix" \
    BLOONIX_ES_HOST="elasticsearch" \
    BLOONIX_ES_PORT="9200" \
    BLOONIX_HOST_ID="1" \
    BLOONIX_HOST_PASSWORD="YourSecretAgentPasswordForHost" \
    BLOONIX_VERSION="0.7-1" \
    BLOONIX_CHECKSUM="1e74e36b45373d3d8cdee9092a42359345f41f20d03f5cbb01def3265405ba3e1586668f38e49d5956bb0816ba19758c16c75c82a3d5e675fd20a8a5cf892201" \
    GOREPLACE_VERSION="1.1.2" \
    GOREPLACE_CHECKSUM="8821544a2f1697664016bf49bd2da45b1d8838bb4ddf326fef795dc4a2735e4716e633bca1404ded10ea54d54fba478220f00e3b1e28e8e964787557803a8aea"

# EPEL
RUN yum -y install epel-release \
 && yum -y update --security \
 && yum -y update \
 && yum -y clean all

# system upgrade 
RUN yum -y update \
 && yum -y upgrade \ 
 # install installations requirements
 && yum -y install wget \
 # install go-replace 
 && wget --timeout=30 --tries=10 --retry-connrefused -O /usr/local/bin/go-replace https://github.com/webdevops/go-replace/releases/download/$GOREPLACE_VERSION/gr-32-linux \
 && sha512sum /usr/local/bin/go-replace \
 && echo $GOREPLACE_CHECKSUM /usr/local/bin/go-replace | sha512sum -c --strict \
 && chmod +x /usr/local/bin/go-replace \
 # install runtime requirements
 && yum -y install java-1.8.0-openjdk \
                   postfix \
                   nginx \
 # Install Bloonix
 && wget --timeout=30 --tries=10 --retry-connrefused -O /tmp/bloonix-release-$BLOONIX_VERSION.noarch.rpm https://download.bloonix.de/repos/centos/7/noarch/bloonix-release-$BLOONIX_VERSION.noarch.rpm \
 && sha512sum /tmp/bloonix-release-$BLOONIX_VERSION.noarch.rpm \
 && echo $BLOONIX_CHECKSUM /tmp/bloonix-release-$BLOONIX_VERSION.noarch.rpm | sha512sum -c --strict \
 && rpm -ivh /tmp/bloonix-release-$BLOONIX_VERSION.noarch.rpm \
 && yum -y install bloonix-webgui \
                   bloonix-server \
                   bloonix-agent \
                   bloonix-wtrm \
                   bloonix-plugins-basic \
                   bloonix-plugins-linux \ 
 # cleanup installations requirements and yum
 && rm -f /tmp/bloonix-release-$BLOONIX_VERSION.noarch.rpm \
 && yum -y remove wget \
 && yum -y clean all \
 # move Bloonix original configuration
 && mv /etc/bloonix/database/main.conf /etc/bloonix/database/main.conf.orginal \
 && mv /etc/bloonix/agent/main.conf /etc/bloonix/agent/main.conf.orginal \
 && mv /etc/bloonix/webgui/main.conf /etc/bloonix/webgui/main.conf.orginal \
 && mv /etc/bloonix/server/main.conf /etc/bloonix/server/main.conf.orginal \
 && mv /etc/bloonix/srvchk/main.conf /etc/bloonix/srvchk/main.conf.orginal 

COPY ["./etc", "/etc"]

RUN chmod +x /etc/epicsoft/*.sh

VOLUME /etc/bloonix

EXPOSE 443 \
       5460

ENTRYPOINT ["/etc/epicsoft/entrypoint.sh"]

HEALTHCHECK --interval=60s --timeout=30s --start-period=20s --retries=3 CMD /etc/epicsoft/healtcheck.sh
